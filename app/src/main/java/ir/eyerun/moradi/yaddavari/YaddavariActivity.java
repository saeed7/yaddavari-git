package ir.eyerun.moradi.yaddavari;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Date;

public class YaddavariActivity extends AppCompatActivity {

    private ListView mListView;
    private YaddavariDbAdapter mDbAdapter;
    private YaddavariSimpleCursorAdapter mCursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yaddavari);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);
        mListView = (ListView) findViewById(R.id.yaddavari_List_View);
        mListView.setDivider(null);
        mDbAdapter = new YaddavariDbAdapter(this);
        mDbAdapter.open();

        if (savedInstanceState == null) {
            //tamami etelaat ro pak kon
            mDbAdapter.deleteAllReminders();
            //ezafe kardane barkhi az dade ha
            insertSomeYaddavariha();
        }

        Cursor cursor = mDbAdapter.fetchAllReminders();
        //from columns defined in the db
        String[] from = new String[]{
                YaddavariDbAdapter.COL_CONTENT};
        //to the ids of views in the layout
        int[] to = new int[]{R.id.row_text};

        mCursorAdapter = new YaddavariSimpleCursorAdapter(
                //context
                YaddavariActivity.this,
                //the layout of the row
                R.layout.yaddavari_radif,
                //cursor
                cursor,
                //from columns defined in the db
                from,
                //to the ids of views in the layout
                to,
                //flag - not used
                0);
        //the cursorAdapter (controller) is now updating the listView (view)
        //with data from the db(model)
        mListView.setAdapter(mCursorAdapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean
                        checked) { }
                @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.cam_menu, menu);
                    return true;
                }
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }
                @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_item_delete_reminder:
                            for (int nC = mCursorAdapter.getCount() - 1; nC >= 0; nC--) {
                                if (mListView.isItemChecked(nC)) {
                                    mDbAdapter.deleteReminderById(getIdFromPosition(nC));
                                }
                            }
                            mode.finish();
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                            return true;
                    }
                    return false;
                }
                @Override
                public void onDestroyActionMode(ActionMode mode) { }

                private int getIdFromPosition(int nC) {
                    return (int)mCursorAdapter.getItemId(nC);
                }
            });
        }

        // zamani ke ma bar roye aytemi khas dar listview  klik mikonim

        //when we click an individual item in the listview
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(YaddavariActivity.this);
                ListView modeListView = new ListView(YaddavariActivity.this);
                String[] modes = new String[] { "Edit Reminder", "Delete Reminder","schedule Reminder" };
                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(YaddavariActivity.this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, modes);
                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();
                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        int nId = getIdFromPosition(masterListPosition);
                        final Yaddavari reminder = mDbAdapter.fetchReminderById(nId);
                        //edit reminder
                        if (position == 0) {
                            fireCustomDialog(reminder);
                        //delete reminder
                        } else if (position == 1){
                            mDbAdapter.deleteReminderById(getIdFromPosition(masterListPosition));
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                        } else {


                            final Date today = new Date();
                            TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                                    Date alarm = new Date(today.getYear(), today.getMonth(), today.getDate(), hour,
                                            minute);
                                    scheduleYaddavari(alarm.getTime(), Yaddavari.getContent());
                                }
                            };


                            new TimePickerDialog( YaddavariActivity.this, null,today.getHours(),today.getMinutes(),  false).show();

                        }
                        dialog.dismiss();
                    }
                });
            }
        });


    }

    private void scheduleYaddavari(long time, String content) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, YaddavariAlarmReceiver.class);
        alarmIntent.putExtra(YaddavariAlarmReceiver.YADDAVARI_TEXT, content);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, broadcast);
    }


    private int getIdFromPosition(int nC) {
        return (int) mCursorAdapter.getItemId(nC);
    }


    private void insertSomeYaddavariha() {
        mDbAdapter.createReminder("kharide ketabe android studio", true);
        mDbAdapter.createReminder("ersale hadiye tavalode pedar", false);
        mDbAdapter.createReminder("raftan be restoran dar jomee", false);
        mDbAdapter.createReminder("baz kardane hesab banki", false);
        mDbAdapter.createReminder("piyadero ro ba bil va khak moratab kon", false);
        mDbAdapter.createReminder("amade kardane barname haye pishrafteye android", true);
        mDbAdapter.createReminder("kharide sandali daftar jadid", false);
        mDbAdapter.createReminder("tamas ba namayandegi khodro", false);
        mDbAdapter.createReminder("ozviyad dar bashgah ro tamdid kon", false);
        mDbAdapter.createReminder("kharide telefone hamrahe jadide htc", true);
        mDbAdapter.createReminder("foroshe telefone hamrahe ghadimi", false);
        mDbAdapter.createReminder("kharide paro baraye poshte bam", false);
        mDbAdapter.createReminder("dar morede pardakhte maliyat ba hesabdar tamas begir", false);
        mDbAdapter.createReminder("ta akhare mah gest ro pardakht kon", false);
        mDbAdapter.createReminder("be hamsaret tamas begir", true);
    }

    private void fireCustomDialog(final Yaddavari yaddavari){
// custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom);
        TextView titleView = (TextView) dialog.findViewById(R.id.custom_title);
        final EditText editCustom = (EditText) dialog.findViewById(R.id.custom_edit_reminder);
        Button commitButton = (Button) dialog.findViewById(R.id.custom_button_commit);
        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.custom_check_box);
        LinearLayout rootLayout = (LinearLayout) dialog.findViewById(R.id.custom_root_layout);
        final boolean isEditOperation = (yaddavari != null);
//this is for an edit
        if (isEditOperation){
            titleView.setText("Edit Reminder");
            checkBox.setChecked(yaddavari.getImportant() == 1);
            editCustom.setText(yaddavari.getContent());
            rootLayout.setBackgroundColor(getResources().getColor(R.color.blue));
        }
        commitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reminderText = editCustom.getText().toString();
                if (isEditOperation) {
                    Yaddavari reminderEdited = new Yaddavari(yaddavari.getId(),
                            reminderText, checkBox.isChecked() ? 1 : 0);
                    mDbAdapter.updateReminder(reminderEdited);
//this is for new reminder
                } else {
                    mDbAdapter.createReminder(reminderText, checkBox.isChecked());
                }
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
                dialog.dismiss();
            }
        });
        Button buttonCancel = (Button) dialog.findViewById(R.id.custom_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_yaddavari, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:
                //create new Reminder
                fireCustomDialog(null);
                return true;
            case R.id.action_exit:
                finish();
                return true;
            default:
                return false;
        }
    }


}

